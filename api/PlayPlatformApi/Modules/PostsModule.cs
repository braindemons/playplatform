using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Validation;
using PlayPlatformApi.Models;

namespace PlayPlatformApi.Modules
{
    public sealed class PostsModule : NancyModule
    {
        private readonly ILogger<PostsModule> _logger;
        private static readonly List<string> Posts = new List<string>
        {
            "Lorem Ipsum",
            "Ipsum Lorem"
        };

        public PostsModule(ILoggerFactory loggerFactory) : base("/api/posts")
        {
            _logger = loggerFactory.CreateLogger<PostsModule>();

            Get("/", parameters => PostsGet());
            Post("/", parameters => PostsPost());
        }

        private ApiResponse<PostsGetResponse> PostsGet()
        {
            var request = this.Bind<PostsGetRequest>();
            var validationResult = this.Validate(request);
            if (!validationResult.IsValid)
            {
                _logger.LogInformation($"Failed to retrieve {request.Amount} posts");
                return new ApiResponse<PostsGetResponse>
                {
                    Success = false,
                    Errors = validationResult.FormattedErrors
                };
            }

            var response = new ApiResponse<PostsGetResponse>
            {
                Success = true,
                Data = new PostsGetResponse {Posts = Posts.TakeLast(request.Amount)}
            };

            _logger.LogInformation($"Retrieved {request.Amount} posts");
            return response;
        }

        private ApiResponse<object> PostsPost()
        {
            var request = this.Bind<PostsPostRequest>();
            var validationResult = this.Validate(request);
            if (!validationResult.IsValid)
            {
                _logger.LogInformation($"Failed to submit post");
                return new ApiResponse<object>
                {
                    Success = false,
                    Errors = validationResult.FormattedErrors
                };
            }

            Posts.Add(request.Post);

            return new ApiResponse<object> {Success = true};
        }
    }
}