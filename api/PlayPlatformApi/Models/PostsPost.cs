using FluentValidation;

namespace PlayPlatformApi.Models
{
    public class PostsPostRequest
    {
        public string Post { get; set; }
    }

    public class PostsPostRequestValidator : AbstractValidator<PostsPostRequest>
    {
        public PostsPostRequestValidator()
        {
            RuleFor(request => request.Post).Length(1, 1024);
        }
    }
}