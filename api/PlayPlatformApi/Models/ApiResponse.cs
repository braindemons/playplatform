using System.Collections.Generic;

namespace PlayPlatformApi.Models
{
    public class ApiResponse<T>
    {
        public bool Success { get; set; }
        public IEnumerable<dynamic> Errors { get; set; }
        public T Data { get; set; }
    }
}