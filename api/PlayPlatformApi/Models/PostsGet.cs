using System.Collections.Generic;
using FluentValidation;

namespace PlayPlatformApi.Models
{
    public class PostsGetRequest
    {
        public int Amount { get; set; }
    }

    public sealed class PostsGetResponse
    {
        public IEnumerable<string> Posts { get; set; }
    }

    public class GetPostsRequestValidator : AbstractValidator<PostsGetRequest>
    {
        public GetPostsRequestValidator()
        {
            RuleFor(request => request.Amount).InclusiveBetween(1, 10);
        }
    }
}