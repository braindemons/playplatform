# PlayPlatform

## Frontend
```
cd app
```

### Build For Production
```
yarn run build
```

### Run Dev Server
```
yarn run dev
```

## Backend
```
cd api/PlayPlatformApi
```

### Run Server
```
dotnet run
```

## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
