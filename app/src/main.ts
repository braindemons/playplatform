import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import { Aurelia, PLATFORM } from 'aurelia-framework';
import config from './config';

import './style/style.scss';

UIkit.use(Icons);

declare global {
    const DEVELOPMENT: boolean
}

export function configure(aurelia: Aurelia) {
    aurelia.use.standardConfiguration();

    if (config.development) {
        aurelia.use.developmentLogging();
    }

    aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app/app')));
}
