import {autoinject} from 'aurelia-framework';
import {Api} from 'app/services/api';

@autoinject
export class Posts {
    posts: string[];
    errors: object[];
    post_text = "";

    constructor(private api: Api) {
        this.api.get_posts({ amount: 10 })
            .then(response => this.posts = response.posts)
            .catch(errors => this.errors = errors);
    }

    post_submit() {
        this.api.post_posts({ post: this.post_text })
            .then(response => {
                this.posts.push(this.post_text);
                this.post_text = "";
            })
            .catch(errors => this.errors = errors);
    }
}
