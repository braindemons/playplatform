import {HttpClient, json} from 'aurelia-fetch-client';
import config from 'config';

interface ApiResponse<T> {
    success: boolean;
    errors: object[];
    data: T;
}

export interface PostsGetRequest {
    amount: number;
}

export interface PostsGetResponse {
    posts: string[];
}

export interface PostsPostRequest {
    post: string,
}

export class Api {
    client = new HttpClient();

    constructor() {
        this.client.baseUrl = config.api_base_url;
    }

    get_posts(request: PostsGetRequest): Promise<PostsGetResponse> {
        return this.fetch_from_api('/posts?amount=' + request.amount);
    }

    post_posts(request: PostsPostRequest): Promise<void> {
        return this.fetch_from_api('/posts', request);
    }
    
    async fetch_from_api<T>(resource: string, post_data?: object): Promise<T> {
        let response;
            try {
                if (post_data) {
                    response = await this.client.fetch(
                        resource, {
                            method: 'post',
                            body: json(post_data)
                        }
                    );
                } else {
                    response = await this.client.fetch(resource);
                }
            } catch {
                throw ["Failed to connect to API"]
            }

            let response_data: ApiResponse<T>;
            try {
                response_data = await response.json();
            } catch {
                throw ["Failed to parse API response"]
            }

            if (!response_data.success) {
                throw response_data.errors
            }

            return response_data.data
    }
}
