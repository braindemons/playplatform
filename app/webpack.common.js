const path = require('path');
const { AureliaPlugin } = require('aurelia-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: [
        // Polyfills needed for the Fetch API, whatg-fetch depends on promise-polyfill
        'promise-polyfill/src/polyfill',
        'whatwg-fetch',
        // Aurelia's entry point, calls into our own main.ts
        'aurelia-bootstrapper',
    ],

    output: {
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },

    resolve: {
        extensions: ['.ts', '.js'],
        modules: ['src', 'node_modules'].map(x => path.resolve(x)),
    },

    module: {
        rules: [
            // css/scss rules are in the dev/prod files
            {
                test: /\.ts$/,
                use: 'awesome-typescript-loader'
            }, {
                test: /\.html$/,
                use: 'html-loader'
            },
        ]
    },

    plugins: [
        new AureliaPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
        }),
        new CopyWebpackPlugin([
            { from: 'static/', to: '' },
        ])
    ],
};
